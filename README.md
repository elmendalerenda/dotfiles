```bash
sudo apt-get install zsh tmux git
```

Manual installation:

https://github.com/robbyrussell/oh-my-zsh

https://github.com/zsh-users/zsh-autosuggestions

https://the.exa.website/install/linux

``` bash
rm ~/.zshrc
cd ~/dotfiles
for f in .*
    ln -s ~/dotfiles/$f ~/$f
rm ~/.git
cp ~/dotfiles/.oh-my-zsh/custom/themes/robbyrussell.zsh-theme ~/.oh-my-zsh/custom/themes/robbyrussell.zsh-theme
```
